
import configparser
Config = configparser.ConfigParser()
Config.read("sample.ini_old")
result=[]
meta={}
for metadata in eval(Config["application"].get("metadatanames")):
    meta[metadata]={"metadata":{"name":metadata,
                "description": metadata + ' desc',
                "filename": metadata + '.tgz'}}
    meta[metadata]["profileMetadata"] = {"name": metadata+"_profile",
                               "filename": "profile.tar.gz"}

    networkdata = eval(Config["networks"].get("networknames"))
    meta[metadata]["clusters"] = [{"selectedClusters":[]}]
    interfaces={"interfaces":[]}
    meta[metadata]["clusters"][0]["provider"]=eval(Config["application"].get("clusterProviderName"))[0]
    interfaces["name"]=eval(Config["application"].get("clusterName"))[0]
    for i in (networkdata):
        for metaname in i.keys():
            if metadata == metaname:
                for network in i[metadata].keys():
                    iface={}
                    iface["networkName"] = network
                    iface["ip"] = i[metadata][network][0]
                    iface["subnet"] = i[metadata][network][1]
                    interfaces["interfaces"].append(iface)
    meta[metadata]["clusters"][0]["selectedClusters"].append(interfaces)
    clusterProviderNames=eval(Config["application"].get("clusterprovidername"))

for i in meta.keys():
    result.append(meta[i])

print(result)
        



